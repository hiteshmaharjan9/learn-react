/* 

Day 1

    A react variable can store tag. 

    We can implement javascript operation inside tag using {}.

    There must be only one wrapper in return statement.

    Anything that is written inside return will display in the browser.

Block Elements
    ->It takes the whole width
    ->It always starts from a new line

Inline Elements
    ->It takes required width

a tag (anchor tag)
    -> a tag is used to create hyperlinks to jump from one page to another.

All tags need to be opened and closed

Image
    -> Always place images or files in the public folder 
    -> . at source of image refers to the public folder

CSS
    ->Inline
        -> Object is used for styling purposes.
        -> You must use camel case convention for the css properties
    ->External
        ->It is a three step process.
            1. Define
            2. Import
            3. Use

In React console will appear at developer panel (f12)
ctrl + shift + i => to open developer tools
ctrl + shift + j => to open console in developer tools


Day 2

Component 
It is a function whose first letter is capital
We call components like tags 
If App() is a component it is called as <App></App>

rafce => react arrow function export component

Props
Values are passed to components in the form of object

No need to write .js when importing functions or variables in react

But while importing .css files we need to put .css

If props is other than string always wrap it by curly braces.

Components are custom tags/ userdefined tags

Components are capital case to separate its tags from html tags

inbuilt props are only supported in inbuilt tags
inbuilt props are not supported in custom/userdefined tags i.e. components

jsx
is an extension
it is a combination of javascript and html (xml)

AJAX
asynchronous javascript

REST API

Day 3

Limitation of curly braces {}
{} does not support comma and multiple expressions as it must only resolve to/return one value
{} does not support if..else statement, for, do...while, while loops and we cannot define variable inside curly braces.

Limitation of ternary operator condition?if true: false
It must have else part

Effect of different data inside html tag.
    -> Boolean values are not shown in browser. For this we have to add some logic
    -> Don't call object inside html tag as children


Day 5

handleImg   (e) => {}                       if you don't need to pass value
handleImg() () => {return ((e) => {})}      if you need to pass value

Day 6

A page will render if state variable is changed

When state variable changes
 -> a component gets rendered such that
        the state variable which is changed holds changed values
        where as other state variable holds previous value

useEffect()
-> useEffect() is an asynchronous function
-> useEffect() is executed in the first render and not in other renders


Day 7
useEffect function will run in the 1st render but from the second render the execution of function depends on its dependency

Day 8
useParams() => to get dynamic route parameter
useSearchParams() => to get query parameter
useNavigate() => to change onClick event


for all     value       e.target.value
check       checked     e.target.checked
radio       checked     e.target.value

*/
//Alt + Shift + A to create a /* */

//<div className="temp">hello</div>
//Here className is a prop and hello is a child

