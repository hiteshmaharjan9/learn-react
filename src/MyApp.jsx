import React from 'react'
import MyRoutes from './practiceComponent/MyRoutes'
import NestingRouteAssignment from './practiceComponent/LearnRouting/NestingRouteAssignment'

const MyApp = () => {
  return (
    <div>
        <NestingRouteAssignment></NestingRouteAssignment>
    </div>
  )
}

export default MyApp