import React from 'react'
import { products } from '../products';

const AssignmentDay4 = () => {
    let task1 = () => {
        let total = products.map((item, i) => {
            return item.price;
        })
        .reduce((prev, cur) => {
            return (prev + cur);
        });
        return (<div>
            The total price of all products is NRs.{total}.
            </div>)
    };

    let task2 = () => {
        let list = products.map((item, i) => {
            return (<div>{item.title} costs NRs.{item.price}.</div>);
        });
        return list;
    };

    let task3 = () => {
        let list = products.reduce((prev, cur) => {
            if (!prev.includes(cur.category))
            {
                prev.push(cur.category);
            }
            return prev;
        }, []).map((item, i) => {
            return (<div>{item}</div>);
        })
        // let out = list.map((item, i) => {
        //     return <div>{item}</div>
        // });
        return list;
    };

    let task4 = () => {
        let list = products.map((item, i) => {
            return (<div>{item.title} is manufactured at {item.manufactureDate}</div>);
        });
        return list;
    };

    let task5 = () => {
        let list = products.filter((item, i) => {
            if (item.isAvailable === true)
                return true;
        })
        .map((item, i) => {
            return (<div>{item.title}</div>);
        });
        return list;
    };

    let task6 = () => {
        // let list = products.filter((item, i) => {
        //     if (item.isAvailable === true)
        //         return true;
        // });
        // let totalPrice = list.map((item, i) => {
        //     return (item.price);
        // })
        // .reduce((prev, cur) => {
        //     return (prev + cur)
        // });

        let totalPrice = products.filter((item, i) => {
            if (item.isAvailable === true)
                return true;
        })
        .map((item, i) => {
            return item.price;
        })
        .reduce((prev, cur) => {
            return (prev + cur);
        });
        return (<div>
            The total price of available products is {totalPrice}.
            </div>);
    };

    let task7 = () => {
        let catList = products.reduce((prev, cur) => {
            if (!prev.includes(cur.category))
            {
                prev.push(cur.category);
            }
            return prev;
        }, []);
        console.log(catList);
        let list = catList.map((cat, i) => {
                let catProd = products.filter((item, i) => {
                    if (item.category === cat)
                        return true;
                })
                .map((item, i) => {
                    return <div>{item.title}</div>
                });
                console.log(catProd);
                catProd.unshift(<h1>{cat}</h1>);
                return catProd;
                // console.log(catProd);
        });
        return list;
    };

  return (
    <div>
        {/* {task2()} */}
        {/* {task1()} */}
        {/* {task3()} */}
        {task4()}
        {/* {task5()} */}
        {/* {task6()} */}
        {task7()}
    </div>
  )
};

export default AssignmentDay4;