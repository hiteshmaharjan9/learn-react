import React, { useEffect, useState } from 'react'

const LearnCleanUpFunction = () => {
    let [count, setCount] = useState(0);

    useEffect(() => {
        console.log("I am useEffect function");
        return () => {
            console.log("I am a cleanup function");
        };
    }, [count]);

    /*
        Cleanup function is a function returned by the callback function in useEffect.
        Cleanup function does not execute in first render.
        But from second render the cleanup function will execute if useEffect function will run

        What happens when useEffect function gets executed?
        -> First cleanup function will run before the code above it


        Component mount and Component unmount
        Component mount is done in first render

        When a component is unmounted nothing gets executed except cleanup function

        LifeCycle of component
        1. Component did mount (first render)
        -> useEffect function will run

        2. Component did update (2nd render) (when state variable changes)
        -> useEffect function will run only if its dependency changes

        3. Component did unmount (component removed)
        -> Nothing gets executed
        -> But cleanup function will execute
    
    */

    let increment = (e) => {
        setCount(count + 1);
    };
  return (
    <div>
        Count = {count}
        <br></br>
        <button onClick={increment}>increment</button>
    </div>
  )
};

export default LearnCleanUpFunction;