import React, { useEffect, useState } from 'react'

const LearnUseEffectHook = () => {
    let [count, setCount] = useState(0);
    let [count2, setCount2] = useState(100);

    let inc = (e) => {
        setCount(count + 1);
    };
    let inc2 = (e) => {
      setCount2(count2 + 1);
    };

    useEffect(() => {
      console.log("I am useEffect 1");
    }, [count, count2]); // this useEffect executes in the first render and from second render when count and count2 variable changes

    useEffect(() => {
      console.log("I am useEffect 2");
    }, [count]); //this useEffect executes in the first render and and from second render when count variable changes

    useEffect(() => {
      console.log("I am useEffect 3");
    }, []); //this useEffect executes in the first render only

    useEffect(() => {
      console.log("I am useEffect 4");
    }); //this useEffect executes in all the renders.

    console.log("...")
  return (
    <div>LearnUseEffectHook
      <br></br>
      Count = {count}
      <br></br>
      <button onClick={inc}>inc</button>
      <br></br>
      Count2 = {count2}
      <br></br>
      <button onClick={inc2}>inc2</button>
    </div>
    
  )
}

export default LearnUseEffectHook