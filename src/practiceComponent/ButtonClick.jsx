import React from 'react'

const ButtonClick = () => {
    let handleClick = (e) => {
        console.log("Button has been clicked");
    };
  return (
    <div>
        <button onClick={handleClick}>
            Click me
        </button>
    </div>
  )
}

export default ButtonClick;