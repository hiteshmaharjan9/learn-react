import React, { useState } from 'react'

const LearnUseStateHook4 = () => {

    let [showImg, setShowImg] = useState(false);

    let handleImg = (isVisible) => {
        return ((e) => {
            setShowImg(isVisible)
        });
        // console.log(isVisible)
    };
    return (
        <div>
            {
                showImg?<img src="./logo192.png" alt="logo"></img>:null
            }
            <br></br>
            <button onClick={handleImg(true)}>Show Image</button><br></br>
            <button onClick={handleImg(false)}>Hide Image</button>
        </div>
    )
}

// handleImg   (e) => {}                       if you don't need to pass value
// handleImg() () => {return ((e) => {})}      if you need to pass value

export default LearnUseStateHook4