import React, { useState } from 'react'

const LearnUseStateHook1 = () => {
    // let name = "hitesh";
    let [name, setName] = useState("hitesh");
    let [age, setAge] = useState(23);
    let handleName = (e) => {
        setName("ram");
    };

    let handleAge = (e) => {
      if (age === 23)
        setAge(50);
      else
        setAge(23);


    };
  return (
    <div>
        <div>
          My name is {name}
          <button onClick={handleName}>Change name</button> 
        </div>
        <div>
          My age is {age}
          <button onClick={handleAge}>Change age</button>

        </div>

    </div>

  )
}

export default LearnUseStateHook1