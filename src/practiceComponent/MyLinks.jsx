import React from 'react'
import { NavLink } from 'react-router-dom'

const MyLinks = () => {
  return (
    <div>
      <NavLink to="/students" style={{margin : "20px"}}>Students</NavLink>
      <NavLink to="/students/create" style={{margin : "20px"}}>Create Students</NavLink>
      <NavLink to="/products" style={{margin : "20px"}}>Products</NavLink>
      <NavLink to="/products/create" style={{margin : "20px"}}>Create Products</NavLink>
    </div>

  )
}

export default MyLinks