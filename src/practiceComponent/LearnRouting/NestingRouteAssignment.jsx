import React from 'react'
import { Navigate, Outlet, Route, Routes } from 'react-router-dom'
import ReadAllProduct from '../product/ReadAllProduct'
import ReadSpecificProduct from '../product/ReadSpecificProduct'
import UpdateProduct from '../product/UpdateProduct'
import UpdateSpecificProduct from '../product/UpdateSpecificProduct'
import { ReadAllStudent } from '../student/ReadAllStudent'
import ReadSpecificStudent from '../student/ReadSpecificStudent'
import CreateStudent from '../student/CreateStudent'
import UpdateStudent from '../student/UpdateStudent'
import MyLinks from '../MyLinks'
import CreateProduct from '../product/CreateProduct'

const NestingRouteAssignment = () => {
  return (
    <div>
        <Routes>
            <Route
                path="/"
                element={<div>
                    <MyLinks></MyLinks>
                    <Outlet></Outlet>
                    {/* <div>This is Footer</div> */}
                    </div>}
            >
                <Route
                    index
                    element={<div>Home Page</div>}
                ></Route>

                <Route
                    path="*"
                    element={<div>404 Page Not Found</div>}
                ></Route>

                {/*Products*/}
                <Route
                    path="products"
                    element={<div><Outlet></Outlet></div>}
                >
                    <Route
                        index
                        element={<ReadAllProduct></ReadAllProduct>}
                    ></Route>

                    <Route
                        path=":id"
                        element={<ReadSpecificProduct></ReadSpecificProduct>}
                    ></Route>

                    <Route
                        path="create"
                        element={<CreateProduct></CreateProduct>}
                    ></Route>

                    <Route
                        path="update"
                        element={<div><Outlet></Outlet></div>}
                    >
                        <Route
                            index
                            element={<UpdateProduct></UpdateProduct>}
                        ></Route>

                        <Route
                            path=":Id"
                            element={<UpdateSpecificProduct></UpdateSpecificProduct>}
                        ></Route>

                    </Route>

                </Route>

                {/*Students*/}
                <Route
                    path="students"
                    element={<div><Outlet></Outlet></div>}
                >
                    <Route
                        index
                        element={<ReadAllStudent></ReadAllStudent>}
                    ></Route>

                    <Route
                        path=":id"
                        element={<ReadSpecificStudent></ReadSpecificStudent>}
                    ></Route>

                    <Route
                        path="create"
                        element={<CreateStudent></CreateStudent>} 
                    ></Route>

                    <Route
                        path="update"
                        element={<div><Outlet></Outlet></div>}
                    >
                        <Route
                            index
                            element={<div><Navigate to="/*"></Navigate></div>}
                        ></Route>

                        <Route
                            path=":Id"
                            element={<UpdateStudent></UpdateStudent>}
                        ></Route>
                    </Route>

                </Route>
            </Route>
        </Routes>

    </div>
  )
}

export default NestingRouteAssignment