// import React from 'react'

// import { useNavigate, useParams, useSearchParams } from 'react-router-dom'

// const CreateProduct = () => {

//     let params = useParams();
//     console.log(params);
//     let [query] = useSearchParams();
//     console.log(query.get("name"));
//     console.log(query.get("subject"));
//     // console.log(query.)

//     let navigate = useNavigate();
    
//     return (
//     <div>CreateProduct
//         <br></br>
//         <button
//         onClick={()=>{navigate("/student")}}
//         >Update</button>
//     </div>
//   )
// }

// export default CreateProduct


import axios from 'axios';
import React, { useState } from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const CreateProduct = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState("");
  let [quantity, setQuantity] = useState("");
  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name : name,
      price: price,
      quantity: quantity
    };
    console.log(data);

    try {
      let result = await axios({
        url: `http://localhost:8000/products`,
        method: "POST",
        data: data
      });
      console.log(result);
      setName("");
      setPrice("");
      setQuantity("");
      // toast("Product created successfully");
      toast.success(result.data.message, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
    } 
    catch (error) {
        toast.error('Product not created', {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
      }
  }

  return (
    <div>
      {/* <ToastContainer position="bottom-right"/> */}
      <ToastContainer
/>
      <form onSubmit={onSubmit}>
        <label htmlFor="name">Name:</label>
        <input 
          type="text" 
          id="name" 
          placeholder="name" 
          value={name} 
          onChange={
            (e) => {
              setName(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="price">Price: </label>
        <input 
          type="number" 
          id="price" 
          placeholder="price" 
          value={price} 
          onChange={
            (e) => {
              setPrice(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="quantity">Quantity: </label>
        <input 
          type="number" 
          id="quantity" 
          placeholder="quantity" 
          value={quantity} 
          onChange={
            (e) => {
              setQuantity(e.target.value)
            }
          }></input>
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default CreateProduct