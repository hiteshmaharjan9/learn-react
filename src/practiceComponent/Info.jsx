import React from 'react'

const Info = ({name, age, favFood, fatherDetail}) => {
  return (
    <div>
        Name = {name} <br></br>
        Age = {age} <br></br>
        favFood = {favFood.map((value, i) => <div>{value}</div>)}
        fatherDetail = {fatherDetail.name}
        
    </div>
  )
}

export default Info